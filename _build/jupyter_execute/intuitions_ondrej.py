#!/usr/bin/env python
# coding: utf-8

# # Foraging models

# In[1]:


from groo.groo import get_root
root_dir = get_root(".hidden_root_foraging")

import sys, os
sys.path.append(os.path.join(root_dir, "scripts", "lib"))
from models import forage_env_hayden

import pandas as pd
import statsmodels.api as sm
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


# ## Hayden 2011
# - initial check

# In[2]:


R,T,Tmat,Mmat,Regmat=forage_env_hayden(1,0.975,[0.1, 0.5], nepi=10)


# In[3]:


# sam's code
Regmat=np.array(Regmat)
y=[Regmat[i][0] for i in range(0,len(Regmat))]
x_Rp=[Regmat[i][1] for i in range(0,len(Regmat))]
x_R=[Regmat[i][2] for i in range(0,len(Regmat))]
x=pd.DataFrame([x_Rp,x_R])
x=x.T

logit_model=sm.Logit(y,x)
result=logit_model.fit()
#print(result.summary2())
coef_Rp=result.params[0]
coef_R=result.params[1]
ratio=np.abs(coef_Rp)/np.abs(coef_R)
#print(ratio)


# In[4]:


df = pd.DataFrame(Regmat, columns=["choice", "rew_patch", "rew_env", "travel_time", "patchID"])
df.head(25)


# #### Patch and global reward/t over 10 patches

# In[5]:


fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(14,4))
ax[0].plot(df["rew_env"])
ax[0].set_title("global reward")
ax[1].plot(df["rew_patch"])
ax[1].set_title("patch reward")



# #### leaving times as a function of patch reward and global reward biases

# In[6]:


# now run the simulation over several different bias inputs
biasA_vec=[1,1.01,1.02,1.03,1.05,1.05] #,1.06,1.07,1.08,1.09,1.1]
biasB_vec=[1,0.99,0.98,0.97,0.96,0.95] #,0.94,0.93,0.92,0.91,0.9]

heatmap_ratio_error = np.full([len(biasA_vec), len(biasB_vec)], np.nan)
heatmap_leaving_times = np.full([len(biasA_vec), len(biasB_vec)], np.nan)

for ibiasA in range(0,len(biasA_vec)):
    for ibiasB in range(0,len(biasB_vec)):
        biasA=biasA_vec[ibiasA]
        biasB=biasB_vec[ibiasB]
        
        R,T,Tmat,Mmat,Regmat=forage_env_hayden(biasA,biasB,[5])
        
        # what do coefficients look like?
        Regmat=np.array(Regmat)
        y=[Regmat[i][0] for i in range(0,len(Regmat))]
        x_Rp=[Regmat[i][1] for i in range(0,len(Regmat))]
        x_R=[Regmat[i][2] for i in range(0,len(Regmat))]
        x=pd.DataFrame([x_Rp,x_R])
        x=x.T
        #x=sm.tools.tools.add_constant(x) # here the constant should capture the influence of travel time
        logit_model=sm.Logit(y,x)
        result=logit_model.fit()
        #print(result.summary2())
        coef_Rp=result.params[0]
        coef_R=result.params[1]
        ratio=np.abs(coef_Rp)/np.abs(coef_R)
        error=np.abs((biasA/biasB)-np.abs(ratio))
        
        heatmap_ratio_error[ibiasA][ibiasB]=error
        heatmap_leaving_times[ibiasA][ibiasB]=np.mean(Tmat[:,1])
        

# 1. what are the leaving times for each of the biases?
# 2D array should be rows x columns (i.e. rows plotted along y, columns along x)
plt.imshow(heatmap_leaving_times, cmap='viridis')
plt.ylabel('Current Patch Reward Rate Bias')
plt.xlabel('Habitat Reward Rate Bias')
labelsY=[str(val) for val in biasA_vec]
labelsX=[str(val) for val in biasB_vec]
plt.yticks(range(0,len(biasA_vec)), labelsY, rotation='horizontal')
plt.xticks(range(0,len(biasB_vec)), labelsX, rotation='vertical')
plt.colorbar()
plt.title('Leaving Times (s)')
plt.show()



# #### Recovery error using logit

# In[ ]:


# 2. What is error of the coefficient ratio (compared with the actual input)?
plt.imshow(heatmap_ratio_error, cmap='viridis')
plt.ylabel('Current Patch Reward Rate Bias')
plt.xlabel('Habitat Reward Rate Bias')
labelsY=[str(val) for val in biasA_vec]
labelsX=[str(val) for val in biasB_vec]
plt.yticks(range(0,len(biasA_vec)), labelsY, rotation='horizontal')
plt.xticks(range(0,len(biasB_vec)), labelsX, rotation='vertical')
plt.colorbar()
plt.title('Coefficient Ratio Error (BiasY/BiasX - coeff.Y/coeff.X)')
plt.show()


# #### Param recvery using optim

# In[ ]:


# Generate N data sets 

